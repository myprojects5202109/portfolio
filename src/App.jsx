import { useState } from 'react'
import reactLogo from './assets/react.svg'
import Home from './components/Home'
import Navbar from './components/Navbar'
import Projects from './components/Projects'
import Skill from './components/Skill'


function App() {

  return (
    <div style={{backgroundImage:'url("https://media.istockphoto.com/id/1225724659/vector/dreamy-smooth-abstract-blue-green-background.jpg?s=612x612&w=0&k=20&c=ng4_9FSFUlUlP0P4B9kZEhWX1Ke7Lt8rpQXZFwCQc34=")' }} className='bg-no-repeat bg-cover bg-center '>
     <Navbar/>
     <Home/>
     <Skill/>
     <Projects/>
        
    </div>
  )
}

export default App
