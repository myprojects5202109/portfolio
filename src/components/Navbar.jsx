import React from 'react'
import { BsFillMoonStarsFill } from "react-icons/bs"
const Navbar = () => {
  return (
    <div className='px-10'>
        <nav className='py-10 mb-12 flex justify-between'>
            <h1 className=' text-xl font-serif text-yellow-100'>developedby Anubhav</h1>
            <ul className='flex items-center'>
                
                <li className='px-3 py-1 bg-gradient-to-r from-cyan-500 to-blue-500 rounded text-zinc-100 hover:-translate-y-1' ><a className=' cursor-pointer ' href='https://drive.google.com/file/d/12hgm43oInbeIT31FDfXX3a-jInJIK9Fw/view?usp=share_link' target="_blank" rel="noopener noreferrer">Resume</a></li>
            </ul>
        </nav>
    </div>
  )
}

export default Navbar
