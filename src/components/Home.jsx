import React from 'react'
import './style.css'
import { AiFillLinkedin} from "react-icons/ai";
import { AiFillGitlab} from "react-icons/ai";
const Home = () => {
  return (
    <div className=' px-17 md:px-20 lg:px-40'>
        <div className=' text-center p-6'>
      <h2 className=' text-5xl py-4 font-medium lg:text-8xl rain' >Anubhav Singh</h2>
     <div className=' overflow-auto'>
      <h3 className='text-xl  md:text-5xl font-mono text-teal-700 p-4 my-3  '>
        <div className='mb-8'>
<span className='  bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>R</span>
<span className='  bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>e</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>a</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>c</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>t</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>J</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>S</span>
</div>

<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>D</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>e</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>v</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>e</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>l</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2   animate-pulse '>o</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>p</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse  '>e</span>
<span className='   bg-slate-50 md:mx-2 w-6 h-6 p-2  animate-pulse '>r</span>
      </h3></div>
      <p className='text-xl text-slate-100 italic'>I’m a recent graduate  with a B.Tech. I’m looking for an opportunity to gain experience in software development.
     I’m here to introduce myself in my professional portfolio.If you’d like to learn more about me, check out my linkedin and Resume.
      </p>
      </div>
      <div className='flex justify-center gap-10  mb-20'>
      <div className=' text-5xl   text-gray-900 my-2  hover:-translate-y-1'><a className=' cursor-pointer' href='https://linkedin.com/in/anubhav-singh-737834217' target="_blank" rel="noopener noreferrer"><AiFillLinkedin/></a></div>
      <div className=' text-5xl   text-gray-900 my-2 hover:-translate-y-1'><a className=' cursor-pointer' href='https://gitlab.com/myprojects5202109' target="_blank" rel="noopener noreferrer"><AiFillGitlab/></a></div>
      </div>
     <div className='flex justify-center h-80  animate-bounce'>
      <img src='./gamer.png'  />
        </div></div>
  )
}

export default Home
