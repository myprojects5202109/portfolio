import React from 'react'
import './style.css'
const Skill = () => {
  return (
    <div>
      <h1 className='text-3xl py-1 my-2 text-center italic  font-semibold' >Skills</h1>
      <div className='p-2 flex flex-wrap justify-center gap-10 ' >
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./html.png' className=' h-24' />
          <h3 className='px-5 font-medium '>HTML5</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./css-3.png' className=' h-24' />
          <h3 className='px-8 font-medium '>CSS3</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./js.png' className=' h-24' />
          <h3 className='px-5 font-medium '>JavaScript</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./physics.png' className=' h-24' />
          <h3 className='px-6 font-medium '>ReactJS</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./next-js.png' className=' h-24' />
          <h3 className='px-6 font-medium '>NextJS</h3>
        </div>
      </div>
      <div className='p-2 flex  flex-wrap justify-center gap-10 ' >
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./bootstrap.png' className=' h-24' />
          <h3 className='px-4 font-medium '>Bootstrap</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./tail.png' className=' h-24' />
          <h3 className='px-3 font-medium '>Tailwindcss</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./java.png' className=' h-24' />
          <h3 className='px-8 font-medium '>Java</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./spring-boot-logo.png' className=' h-24' />
          <h3 className='px-4 font-medium '>SpringBoot</h3>
        </div>
        <div className=' box-border shadow-lg h-33 w-32 p-3 rounded-xl  bg-slate-50 bg-opacity-50 mb-2 flipY'>
          <img src='./mongo.png' className=' h-24' />
          <h3 className='px-4 font-medium '>MongoDB</h3>
        </div>
      </div>
    </div>
  )
}

export default Skill
