import React from 'react'

const Projects = () => {
  return (
    <div className='p-3'> 
         <h1 className='text-3xl py-1 my-3 text-center italic  font-semibold' >Projects</h1>
         <div className=' md:flex justify-center gap-10 '> 
            <div className=' box-border shadow-xl rounded  max-w-sm py-5 hover:-translate-y-4 mb-3 bg-slate-50 bg-opacity-50'>
     <a href='https://falconefind.netlify.app/' target="_blank" rel="noopener noreferrer" >  <img src='./falcone.png'  /></a>
     <h2 className='font-bold text-xl mb-2 my-2 px-2'>Finding Falcone</h2>
     <p className='italic  font-medium text-gray-700 px-2'>A small fun activity to send spaceships to find the escaped queen.</p>

            </div>
            <div className=' box-border shadow-xl rounded  max-w-sm py-5 hover:-translate-y-4 mb-3 bg-slate-50 bg-opacity-50'>
     <a href='https://food-wala-d6b0a.web.app/' target="_blank" rel="noopener noreferrer" >  <img src='./FoodWala.png'  /></a>
     <h2 className='font-bold text-xl mb-2 my-2 px-2'>Food Wala</h2>
     <p className='italic  font-medium text-gray-700 px-2'>A restaurant app from where you can order food. It is made with ReactJS and Google Firebase.</p>

            </div>
            <div className=' box-border shadow-xl rounded  max-w-sm py-5 hover:-translate-y-4 mb-3  bg-slate-50 bg-opacity-50'>
     <a href='https://my-flix-ten.vercel.app/' target="_blank" rel="noopener noreferrer">  <img src='./MyFlix.png'  /></a>
     <h2 className='font-bold text-xl mb-2 my-2 px-2'>MyFlix</h2>
     <p className='italic  font-medium text-gray-700 px-2' >A NextJS app were you can read about trending anime and movies.</p>

            </div>
         </div>
    </div>
  )
}

export default Projects
